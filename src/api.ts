
interface Api {

	Sensors: SensorsType
	RefusedErrors: RefusedErrorsType
	util: Util

	sendDataBoolean(idSensor: number, data: boolean[], error?: (error: { msg: string }) => void): void
	sendDataInt(idSensor: number, data: number[], error?: (error: { msg: string }) => void): void
	sendDataFloat(idSensor: number, data: number[], error?: (error: { msg: string }) => void): void
	sendDataString(idSensor: number, data: string, error?: (error: { msg: string }) => void): void
	sendFile(name: string, mimeType: string, data: string, error?: (error: { msg: string }) => void): void
	startSensors(): void
	stopSensors(): void
	getSensors(success: (result: { sensors: number[] }) => void): void
	measure(measure: number[], error?: (error: { msg: string }) => void): void
	lock(): void
	unlock(): void
	readBucket(bucket: string, success?: (result: string) => void, error?: (error: { msg: string }) => void): void
	deleteBucket(bucket: string, success?: () => void, error?: (error: { msg: string }) => void): void
	writeBucket(bucket: string, data: string, success?: () => void, error?: (error: { msg: string }) => void): void
	listBucket(success?: (result: { buckets: string[] }) => void, error?: (error: { msg: string }) => void): void

}

interface SensorsType {
	ACCELEROMETER: number,
	GRAVITY: number,
	GYROSCOPE: number,
	LIGHT: number,
	LINEAR_ACCELERATION: number,
	MAGNETIC_FIELD: number,
	SPEAKER: number,
	VIBRATION: number,
	CAMERA: number,
	CAMERA_FRONT: number,
	MICROPHONE: number,
	KEYBOARD: number,

	ECHO_REQ: number,
	ECHO_RES: number,
}

interface RefusedErrorsType {
	UNKNOW_ERROR: number,
	ENOUGH_DEVICES: number,
	LACK_OF_SENSORS: number,
}

interface Util {
	toArrayBoolean(size: number, data: string): boolean[]
	toArrayInt(size: number, data: string): number[]
	toArrayFloat(size: number, data: string): number[]
	arrayToString(data: string): string
}


declare const API: Api
