const path = require('path')
const commandLineArgs = require('command-line-args')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const TSLintPlugin = require('tslint-webpack-plugin')


let argv = commandLineArgs([{ name: 'mode', type: String, defaultValue: 'development' },
							{ name: 'environment', type: String, defaultValue: 'dev' },
							{ name: 'platform', type: String, defaultValue: 'desktop' },
							{ name: 'target-src', type: String, defaultValue: 'build/undefined' }],
							{ partial: true })



// Extension by enviroments ---------------------------------------

let extensions = ['tsx', 'ts', 'js', 'scss']
let calcResolves = () => {
	let environment = argv.environment
	let platform = argv.platform

	let res = []
	for (let ext of extensions) {
		res.push('.' + platform + '.' + environment + '.' + ext)
		res.push('.' + environment + '.' + ext)
		res.push('.' + platform + '.' + ext)
		res.push('.' + ext)
	}

	return res
}



// Webpack config -----------------------------------------------

const config = (entry, name, target) => {

	let isProduction = argv.mode == 'production'

	let config = {
		mode: isProduction ? 'production' : 'development',
		target,
		resolve: {
			extensions: calcResolves(),
			modules: [path.resolve(__dirname, 'src'), 'node_modules']
		},
		context: __dirname,
		output: {
			path: path.join(__dirname, argv['target-src']),
			filename: '[name].js',
			publicPath: ''
		},
		devServer: {
			host: '0.0.0.0'
		},
		module: {
			rules: [
				{
					test: /\.scss$/,
					exclude: /node_modules/,
					use: [
						'style-loader',
						'css-loader',
						{
							loader: 'sass-loader',
							options: {
								outputStyle: 'compressed'
							}
						}
					]
				},
				{
					test: /\.tsx?$/,
					use: 'ts-loader',
					exclude: /node_modules/
				}
			]
		},
		plugins: [
			new CleanWebpackPlugin(),
			new CopyWebpackPlugin([
				{ from: 'src/assets', to: 'assets' }
			])
		],
		optimization: {
			minimizer: []
		}
	}

	if(isProduction) {
		config.optimization.minimizer.push(new TerserPlugin())
		config.plugins.push(new TSLintPlugin({
			files: ['./src/**/*.ts', './src/**/*.tsx']
		}))
	} else {
		config.plugins.push(
			new HTMLWebpackPlugin({
				filename: 'index.html',
				template: './src/index.html',
			}))
	}

	config.entry = {}
	config.entry[name] = entry

	return config;
}

module.exports = () => [config('./src/index.ts', 'gamepad', 'web')]
