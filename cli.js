const commandLineArgs = require('command-line-args')
const child = require('child_process')

// Arguments -------------------------------------------

const mainCommand = commandLineArgs([{ name: 'command', defaultOption: true }], { stopAtFirstUnknown: true })


// Main -------------------------------------------

switch (mainCommand.command) {

	case 'serve':
		child.exec('webpack-dev-server --port 8081 --inline=true')
		const server = child.exec('webpack-dev-server --port 8082 --inline=false')
		server.stdout.pipe(process.stdout)
		server.stderr.pipe(process.stdout)
		break

}


